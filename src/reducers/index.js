import { combineReducers } from 'redux'

import todos, * as fromTodos from './todos'

export default combineReducers({
	todos
})

// Selectors
export const getTodos = (state) => fromTodos.getTodos(state)