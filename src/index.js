import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import * as serviceWorker from './serviceWorker';

import store from './store.js'

// Components
import AddTodo from './components/addTodo'
import TodoList from './components/todoList'

import { Provider } from 'react-redux'


ReactDOM.render(
	<Provider store={store}>
		<div>
			<header>
				<h1>Todo App</h1>
			</header>
			<AddTodo />
			<TodoList />
		</div>
	</Provider>
, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
