import React, { Component } from 'react'
import { connect } from 'react-redux'

// Action
import * as todoActions from '../actions/todos'

class AddTodo extends Component {

	state = {
		todoText: ''
	}

	onFormSubmit = (e) => {
		e.preventDefault()

		const { todoText } = this.state

		if (todoText === '') {
			alert('Please add todo details!')
			return
		}

		this.props.addTodo(todoText)
		this.setState({todoText: ''})
	}

	render() {

		const { todoText } = this.state

		return (
			<form onSubmit={(e) => this.onFormSubmit(e)}>
				<input type="text" onChange={(e) => this.setState({ todoText: e.target.value })} value={todoText} />
				<button type="submit">Add</button>
			</form>
		)
	}

}

const mapStateToProps = (state) => ({

})

const mapDispatchToProps = (dispatch) => ({
	addTodo: (text) => dispatch(todoActions.setTodo(text))
})

export default connect(mapStateToProps, mapDispatchToProps)(AddTodo)