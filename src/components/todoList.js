import React, { Component } from 'react'
import { connect } from 'react-redux'

import * as select from '../reducers'
import * as todoActions from '../actions/todos'

class TodoList extends Component {

	handleTodoClick = (id) => {
		this.props.toggleTodo(id)
	}

	render() {

		const { todos } = this.props

		return (
			<ul className="todoList">
				{todos.map(todo => {
					return <li
							onClick={() => this.handleTodoClick(todo.id)}
							key={todo.id}
							className={todo.completed ? 'completed' : ''}
						>
							{todo.text}
							<span>{todo.completed ? '☑️' : ''}</span>
						</li>
				})}
			</ul>
		)
	}

}

const mapStateToProps = (state) => ({
	todos: select.getTodos(state)
})

const mapDispatchToProps = (dispatch) => ({
	toggleTodo: (id) => dispatch(todoActions.toggleTodo(id))
})

export default connect(mapStateToProps, mapDispatchToProps)(TodoList)